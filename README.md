# Barcino Bites docker

## Barcino bites docker
```
-nginx
-strapi
-vue-frontend
-mysql
```

## Setup:
-Clone repo:

```git clone git@gitlab.com:djanramadan/barcino-bites-docker.git --branch develop```

Change .env.example as .env -

    ```cp .env.example .env ```

-Add url for app and api(Optional)
```
echo ` wwww.yourappurl.loc yourappurl.loc yourapiurl.loc`
```
or app will run http://localhost

-Run

    ```./composer update ```

## Start Project

    in project dir run :

    ```docker-compose up -d --build```