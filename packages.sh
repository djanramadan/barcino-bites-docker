declare -a MODULES

strapi=("strapi" "develop" "https://gitlab.com/djanramadan/barcino-bites-strapi" "")
app=("app" "develop" "https://gitlab.com/djanramadan/barcino-bites-frontend" "")

MODULES=(
    strapi[@]
    app[@]
)
