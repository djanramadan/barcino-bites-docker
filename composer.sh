#!/usr/bin/env bash

HERE=$(pwd) # we assume this script is run as ./dc.sh - if not, bad things may happen
NORESET=0

# Allow deployment to a specified relative path

if [ "${2}" == "" ]
then
    RELATIVE_PATH="."
else
    RELATIVE_PATH="${2}"
fi

if [ "${3}" == "no-reset" ]
then
    NORESET=1    
fi

. packages.sh


update() {
    create_env

    COUNT=${#MODULES[@]}
    echo "Deploying ${COUNT} modules to ${HERE}/${RELATIVE_PATH}"

    for ((i=0; i<$COUNT; i++))
    do
        DIR="${RELATIVE_PATH}/${!MODULES[i]:0:1}"
        BRANCH=${!MODULES[i]:1:1}
        REPO=${!MODULES[i]:2:1}
        TAG=${!MODULES[i]:3:1}

        if [ -d ./${DIR} ]; then
            cd ./${DIR}
            if [ $NORESET == 0 ]
            then
                echo "hard resetting"
                git fetch --all
                git reset --hard origin/${BRANCH}
            fi

            echo "Updating ${DIR}"
            if [ ${TAG} ]; then
                git checkout tags/${TAG}    
            else
                git checkout ${BRANCH}
                git pull origin ${BRANCH}   
            fi                     
            cd ${HERE}
        else
            echo "Cloning ${REPO} into ${DIR}"
            git clone ${REPO} --branch ${BRANCH} ${DIR}
            cd ${DIR}
            if [ ${TAG} ]; then
                git checkout tags/${TAG}    
            fi
            cd ${HERE}
        fi
    done
}

create_env() {
    if [[ ${#@} -gt 0 && ${1} == "force" || ! -f .env ]]
    then
        prt "Preparing .env"
        ENVSUBST_PATH=$(which envsubst)
        if [ ${#ENVSUBST_PATH} -gt 0 ]
        then
            run "${ENVSUBST_PATH} < .env.dist > .env"
        else
            warn "envsubst not found, copying instead"
            run "cp .env.dist .env"
        fi

        echo "HOSTUSERID=$(id -u)" >> .env
        echo "HOSTGROUPID=$(id -g)" >> .env
    else
        prt "SKIP: .env - already exists"
    fi
}

CMD="${1}"
[ ${#CMD} -lt 1 ] && err "No command given" && exit 1

case ${CMD} in
    update)
        update
        ;;

    *)
    err "Unrecognised command \"${CMD}\""
    exit 1
    ;;
esac